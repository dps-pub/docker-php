FROM php:fpm

MAINTAINER Thomas Albrighton <tom@dps.com.au>

RUN apt-get update \
    && apt-get install -y automake docbook libtool docbook2x tesseract-ocr

RUN cd /usr/src \
    && curl -O ftp://ftp.freetds.org/pub/freetds/stable/freetds-1.00.55.tar.gz \
    && tar -zxvf freetds-* \
    && rm -rf freetds-*.tar.gz \
    && cd freetds-* \
    && ./configure \
        --prefix=/usr/local/freetds \
        --sysconfdir=/usr/local/etc/freetds \
    && make \
    && make install \
    && cd .. \
    && rm -r freetds-*

RUN curl -sL https://github.com/libexpat/libexpat/archive/R_2_2_4.tar.gz > libexpat.tar.gz \
    && gzip -d libexpat.tar.gz \
    && tar -xf libexpat.tar \
    && rm libexpat.tar \
    && cd libexpat*/expat \
    && mkdir m4 \
    && ./buildconf.sh \
    && ./configure \
    && make \
    && make install \
    && cd ../.. \
    && rm -r libexpat*


RUN cd /usr/src \
    && curl -O http://apache.mirror.anlx.net/apr/apr-1.6.2.tar.gz \
    && tar -zxvf apr-* \
    && rm -rf apr-*.tar.gz \
    && cd apr-* \
    && ./configure \
    && make \
    && make install \
    && cd .. \
    && rm -r apr-*

RUN cd /usr/src \
    && curl -O http://apache.mirror.anlx.net/apr/apr-util-1.6.0.tar.gz \
    && tar -zxvf apr-util-* \
    && rm -rf apr-util-*.tar.gz \
    && cd apr-util-* \
    && ./configure --with-apr=/usr/local/apr \
    && make \
    && make install \
    && cd .. \
    && rm -r apr-util-*

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
    && apt-get install -y \
        libfreetype6-dev \
        libjpeg-dev \
        libmcrypt-dev \
        libpng12-dev \
        mysql-client \
        nodejs \
        git \
        vim \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure gd \
        --with-freetype-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure pdo_dblib \
        --with-pdo-dblib=/usr/local/freetds

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN apt-get update \
    && apt-get -y install \
            libmagickwand-dev \
        --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-install -j$(nproc) \
        mcrypt \
        zip \
        pcntl \
        pdo_mysql \
        pdo_dblib \
        gd \
        opcache

RUN curl -sL "https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs921/ghostscript-9.21-linux-x86_64.tgz" > gs.tgz \
    && gzip -d gs.tgz \
    && tar -xf gs.tar \
    && cp ghostscript-*/gs-* /usr/local/bin/gs \
    && rm -r ghostscript-* \
    && rm gs.tar


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && useradd 1000 && mkdir /home/1000 && chown 1000:1000 -R /home/1000

COPY .bashrc /home/1000/.bashrc
COPY .bashrc /home/root/.bashrc

COPY root /

WORKDIR "/var/www"
